<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- CSS flickity -->
	<!-- <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
	<link rel="stylesheet" href="/path/to/flickity.css" media="screen"> -->

	<!-- owl carousel
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->

	<!-- w3school -->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

	<!-- shorcut favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets/img/logoip1.jpg') ?>">

	<!-- boostrap 4.6 css -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
		integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

	<!-- font awesome cdn link -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

	<!-- custom css file link -->
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    

	<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

	<!-- testimoni swiper js -->
	<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css">
	<!-- <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/> -->

	<!-- swiper cdn styling -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.4.1/swiper-bundle.min.css" />

	<!-- font awesome bootstrap -->
	<link rel="stylesheet"
		href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css">

	<!-- bootstrap cdnjs -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.6.1/js/bootstrap.min.js">

	<!-- bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- slick slider cdnjs -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js">
	<link rel="stylesheet" type="text/css" href="slick/slick.css" />
	<!-- Add the new slick-theme.css if you want the default styling -->
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />

	<title>IP1.co.id</title>
</head>

<body>


	<!-- header starts -->
	<header id="home1" class="header">

		<div class="header1">
			<div class="right">
				<a href="Cekarea.html">Cek Area</a>
				<a href="login.html">Login</a>
				<a href="#"><i class="fas fa-mobile-screen-button fa-l"></i> IP1 App</i></a>
				<!-- <a href="#">Advertise</a> -->
			</div>
		</div>

		<div class="container">
			<div class="header-main">
				<div class="logo">
					<img src="img/logoip1.jpg">
					<a href="#">Infinity Plus One</a>
				</div>
				<div class="open-nav-menu">
					<span><i class="fas fa-bars"></i></span>
				</div>
				<!-- ketika dibuka background menjadi gelap -->
				<div class="menu-overlay"></div>
				<!-- navigation menu starts -->
				<nav class="nav-menu">
					<div class="close-nav-menu">
						<span><i class="fas fa-close"></i></span>
					</div>
					<ul class="menu">
						<li class="menu-item menu-item-has-children">
							<a href="index.html" data-toggle="sub-menu" class="highlight">Home</i></a>
						</li>
						<li class="menu-item">
							<a href="Aboutus.html" >About Us</a>
						</li>
						<li class="menu-item">
							<a href="#services">Services</a>
						</li>
						<li class="menu-item menu-item-has-children">
							<a href="#" data-toggle="sub-menu">Product <i class="fas fa-sort-down"></i></a>
							<ul class="sub-menu">
								<li class="menu-item"><a href="#">Home 1</a></li>
								<li class="menu-item"><a href="#">Home 2</a></li>
								<li class="menu-item"><a href="#">Home 3</a></li>
							</ul>
						</li>
						<li class="menu-item">
							<a href="kontak.html">Contact</a>
						</li>
						<li class="menu-item menu-item-has-children">
							<a href="article.html" data-toggle="sub-menu">Artikel<i class="fas fa-sort-down"></i></a>
							<ul class="sub-menu">
								<li class="menu-item"><a href="Gallery.html">Gallery</a></li>
							</ul>
						</li>
						<!-- <li class="menu-item">
                            <a href="#">Cek Area</a>
                        </li> -->
						<li class="menu-item">
							<a href="#">Carreer</a>
						</li>
					</ul>
				</nav>
				<!-- navigation menu end -->
			</div>
		</div>
	</header>
	<!-- header end -->